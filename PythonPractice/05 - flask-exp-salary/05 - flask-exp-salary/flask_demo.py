from flask import flask
from flask_restplus import Resource, Api
import joblib

app = flask(__name__)
api = Api(app)

model = joblib.load('exp_salary.joblib')

@api.route('/<int:years_exp')
class Salary(Resource):

    def get(self, years_exp):
        print('Query for :' + str(years_exp))
        salary = model.predict([[ years_exp ]])
        return {'salary' : salary[0]}

if __name__ == '__main__':
    print('hello')
    app.run(debug = True)