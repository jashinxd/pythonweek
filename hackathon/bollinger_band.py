import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import pandas_datareader as pdr
import datetime
import price_data


def get_price(ticker):

    return price_data.get_price(ticker)['price']


def generate_rolling_avg(ticker):

    start_date = datetime.datetime.today() - datetime.timedelta(365)
    end_date = datetime.datetime.today() - datetime.timedelta(1)
    df = pdr.get_data_yahoo(ticker, start=start_date, end=end_date)

    df['30d mavg'] = df['Adj Close'].rolling(window=21).mean()
    df['30d std'] = df['Adj Close'].rolling(window=21).std()

    return df


def calculate_bands(ticker):

    df = generate_rolling_avg(ticker)

    df['Upper Band'] = df['30d mavg'] + (df['30d std'] * 2)
    df['Lower Band'] = df['30d mavg'] - (df['30d std'] * 2)

    return df

def plotBollBand(df, securityName, attribute = 'Adj Close', window = 30, start="08-01-19", end="08-01-20"):
    mavgCol = str(window) + "d mavg"
    stdCol = str(window) + "d std"
    plt.style.use('fivethirtyeight')
    fig = plt.figure(figsize=(12,6))
    ax = fig.add_subplot(111)
    x_axis = df[start:end].index.get_level_values(0)
     # Plot shaded 21 Day Bollinger Band 
    ax.fill_between(x_axis, df[start:end]['Upper Band'], df[start:end]['Lower Band'], color='grey')

    ax.plot(x_axis, df[start:end][attribute], color='blue', lw=2)
    ax.plot(x_axis, df[start:end][mavgCol], color='black', lw=2)
     # Set Title & Show the Image
    title = str(window) + ' Day Bollinger Band For ' + securityName
    ax.set_title(title)
    ax.set_xlabel('Date (Year/Month)')
    ax.set_ylabel('Price')
    ax.legend()
    plt.savefig('static/'+securityName+start+end+".jpg")
    return 'static/'+securityName+start+end+".jpg"

def advice(ticker):

    price = get_price(ticker)
    df = calculate_bands(ticker)
    print(df.tail())

    if price > df.iloc[-1]['Upper Band']:
        return 'sell'
    elif price < df.iloc[-1]['Lower Band']:
        return 'buy'
    return 'hold'
