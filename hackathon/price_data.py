import datetime
import logging

import pandas_datareader as pdr

LOG = logging.getLogger(__name__)


def get_price(ticker):

    start_date = datetime.datetime.now() - datetime.timedelta(1)
    df = None
    try:
        df = pdr.get_data_yahoo(ticker, start_date)
        df['Date'] = df.index
    except Exception as ex:
        LOG.error('Error getting data: ' + str(ex))

    LOG.error('Recieved price data:')
    LOG.error(str(df))

    return {'ticker': ticker,
            'date': df.iloc[-1]['Date'].strftime("%m/%d/%Y"),
            'price': df.iloc[-1]['Close']}


def get_price_date(ticker, start_date, end_date):
    price_data = []
    try:
        df = pdr.get_data_yahoo(ticker, start_date, end_date)[['Close']]
    except Exception as ex:
        LOG.error('Error getting data: ' + str(ex))
        return {}

    LOG.error('Recieved price data:')
    LOG.error(str(df))

    df['Date'] = df.index

    for index, row in df.iterrows():
        price_data.append({'date': index.strftime("%m/%d/%Y"),
                            'price': row['Close']})
    return price_data


def get_price_two(ticker):

    start_date = datetime.datetime.today() - datetime.timedelta(1)

    df = pdr.get_data_yahoo(ticker, start_date)

    df['Date'] = df.index

    return {'ticker': ticker,
            'date': df.iloc[-1]['Date'].strftime("%m/%d/%Y"),
            'price': df.iloc[-1]['Close']}