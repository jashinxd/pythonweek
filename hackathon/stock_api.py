from datetime import datetime
import logging
import bollinger_band
import price_data

from flask import Flask, render_template, request
from flask_restplus import Resource, Api

LOG = logging.getLogger(__name__)

app = Flask(__name__)
api = Api(app)

@api.route('/v1/price/<string:tickers>/<string:start_date>/<string:end_date>')
#@api.param('tickers', 'The stock ticker to get a price for')
# @api.param('start_date', 'The first date requested')
# @api.param('end_date', 'The last date requested')
class Price(Resource):
    def get(self, tickers, start_date, end_date):
        price_info = []
        tickers_list = tickers.strip().split(',')
        print(tickers_list)
        start_date_formatted = datetime.strptime(start_date, "%m-%d-%y").date()
        end_date_formatted = datetime.strptime(end_date, "%m-%d-%y").date()
        for ticker in tickers_list:
            price_info.append({ticker : price_data.get_price_date(ticker, start_date_formatted, end_date_formatted)})
        return price_info

# @api.route('/advice/<string:ticker>')
# class Advice(Resource):

#     def get(self, ticker):
#         print('Requesting advice for: ' + str(ticker))
#         advice = bollinger_band.advice(ticker)
#         return {'advice': advice}

@app.route('/index')
def index():
    return render_template('index.html')

@app.route('/result', methods=['POST'])
def result():
    ticker = request.form["ticker"]
    advice = bollinger_band.advice(ticker)
    df = bollinger_band.calculate_bands(ticker)
    imgpath = bollinger_band.plotBollBand(df, ticker)
    return render_template('bollinger.html', stock=ticker , advice=advice, imgpath=imgpath)

if __name__ == "__main__":
    app.run(debug=True, port=8080)
